var express = module.require("express");
var bodyParser = module.require("body-parser");
var app = express();

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}));

app.use(express.static(__dirname + '/www'));

app.listen(3000, function(){
	console.log("listening on port 3000!");
});