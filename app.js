var express = module.require("express");
var bodyParser = module.require("body-parser");
var app = express();

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}));

app.use(express.static(__dirname + '/www'));

var users = [{ime: "Matija", prezime: "Bogdanović"},
			 {ime: "Marija", prezime: "Katić"}];

app.listen(3000, function(){
	console.log("listening on port 3000!");
});

app.get("/users", function (request, response) {
	response.json(users);
});

app.post("/users", function (request, response) {
	users.push(request.body);
	response.status(200);
});

app.post("/delete", function (request, response) {
	var ime = request.body.ime;
	var prezime = request.body.prezime;

	var userIndex = -1;
	for (index in users){
		var user = users[index];
		if (user.ime == ime && user.prezime == prezime) {
			userIndex = index;
			break;
		}
	}

	if (userIndex != -1) {
		users.splice(userIndex, 1);
	}

	response.json(users);
});

app.post("/editUser", function (request, response) {
	var index = request.body.index;
	var ime = request.body.ime;
	var prezime = request.body.prezime;

	users[index].ime = ime;
	users[index].prezime = prezime;
	
	response.json(users);
});
