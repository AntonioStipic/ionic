// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic'])

.run(function($ionicPlatform) {
	$ionicPlatform.ready(function() {
		if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
  }
  if(window.StatusBar) {
  	StatusBar.styleDefault();
  }
});
})
.controller("tableController", function($scope, $ionicModal, $http) {
	var self = this;
	self.users = [];
	$http.get("/users").then(function (response) {
		var user = [];
		for (var i = 0; i < response.data.length; i++) {
			self.users.push(response.data[i]);
		}
	});
	
	//this.a = ["Matija Bogdanović", "Zvjezdana Svalina", "Marija Katić"];

	$ionicModal.fromTemplateUrl('/my-modal.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.modal = modal;
	});

	$ionicModal.fromTemplateUrl('/edit-modal.html', {
		scope: $scope,
		animation: 'slide-in-left-right'
	}).then(function(editModal) {
		$scope.editModal = editModal;
	});

	$scope.buttonAction = function() {
		$scope.modal.show();
	}

	$scope.closeModal = function() {
		$scope.modal.hide();
	};

	$scope.openEditModal = function(){
		$scope.editModal.show();
	}

	$scope.closeEditModal = function(){
		$scope.editModal.hide();
	}

	$scope.modalButtonAction = function(ime, prezime) {
		self.users.push({ime: ime, prezime: prezime});
		var user = {ime: ime, prezime: prezime};
		$http.post("/users", user).then(function (response) {
		});
		$scope.modal.ime = "";
		$scope.modal.prezime = "";
		$scope.closeModal();

	}

	$scope.delete = function(user) {
		$http({
			method: "POST",
			data: user,
			url: "/delete"
		}).then(function successCallback(response) {
			self.users = response["data"];
		}, function errorCallback(response) {

		});
	}

	$scope.editUserModal = function (user) {
		$scope.openEditModal();
		self.tmpIme = user["ime"];
		self.tmpPrezime = user["prezime"];
		$scope.editModal.ime = user["ime"];
		$scope.editModal.prezime = user["prezime"];
	}

	$scope.editModalButtonAction = function (ime, prezime) {
		var userIndex = -1;
		for (index in self.users){
			var user = self.users[index];
			if (user.ime == self.tmpIme && user.prezime == self.tmpPrezime) {
				userIndex = index;
				break;
			}
		}

		var user = {ime: ime, prezime: prezime, index: userIndex};
		$http({
			method: "POST",
			data: user,
			url: "/editUser"
		}).then(function successCallback(response) {
			self.users = response["data"];
		}, function errorCallback(response) {

		});
		$scope.editModal.ime = "";
		$scope.editModal.prezime = "";
		$scope.closeEditModal();
		
	}

})